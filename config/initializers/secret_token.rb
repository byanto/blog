# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyApp::Application.config.secret_key_base = '2f4f42e10e2a959a124e554d91acf63b1df15e40d243e3fc700706ea10e6b88b351b1adbab88345d7f5569f75771a283d5db4b50f0df1ff9153b6620105faa6e'
